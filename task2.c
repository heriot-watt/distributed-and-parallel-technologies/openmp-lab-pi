#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define NB_THREADS 10
#define NB_STEPS 10000

double* allocVector(int n) {
  return (double*) malloc(n * sizeof(double));
}

int main(int argc, char **argv) {
  int num_t, num_threads, id;
  double start, end, x, step, pi = 0;
  double *sum;

  num_t = argc == 2 ? strtol(argv[1], NULL, 10) : NB_THREADS;

  omp_set_num_threads(num_t);
  sum = allocVector(num_t);
  step = 1.0 / NB_STEPS;

  start = omp_get_wtime();

  #pragma omp parallel \
    shared(step, sum) \
    private(num_threads, id, x)
  {
    num_threads = omp_get_num_threads();
    id = omp_get_thread_num();

    sum[id] = 0;

    for (int i = id; i < NB_STEPS; i+=num_threads) {
      x = (i+0.5) * step;
      sum[id] += 4.0 / (1.0 + x*x);
    }
  }

  for (int i = 0; i < num_t; i++) 
    pi += sum[i] * step;

  end = omp_get_wtime();

  printf("Execution time: %.6f sec\n", end-start);
  printf("PI ~= %lf\n", pi);
  return 0;
}
