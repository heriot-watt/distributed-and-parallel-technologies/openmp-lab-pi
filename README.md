# Compiling

Run:

    make
    
# Running

Specify how many OpenMP threads as an argument to the executable. For
example:

    srun ./pi_openmp 4

You can probably ignore the "Exited with exit code 26" error.