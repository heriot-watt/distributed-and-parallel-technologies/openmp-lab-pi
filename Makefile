LDFLAGS += -fopenmp -std=c99

TARGETS = task1 task2 task3 task4 task5

all: $(TARGETS)

task1: task1.c
	$(CC) $(LDFLAGS) -g -o $@ $^

task2: task2.c
	$(CC) $(LDFLAGS) -g -o $@ $^

task3: task3.c
	$(CC) $(LDFLAGS) -g -o $@ $^

task4: task4.c
	$(CC) $(LDFLAGS) -g -o $@ $^

task5: task5.c
	$(CC) $(LDFLAGS) -g -o $@ $^

clean:
	$(RM) $(TARGETS)

