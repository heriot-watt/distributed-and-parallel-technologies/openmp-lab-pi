#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) 
{
  int num_steps = 10000;
  double step, pi, sum[1000];
  int num_t, i;

  step = 1.0/num_steps;
  omp_set_num_threads(strtol(argv[1], NULL, 10));
#pragma omp parallel
  {  
    int i, id;
    int num_threads;
    double x;
    id = omp_get_thread_num();
    num_threads = omp_get_num_threads();
    if(id==0)
      num_t = num_threads;
    sum[id] = 0;
    for(i=id; i<num_steps; i+=num_threads) {
      x = (i+0.5) * step;
      sum[id] += 4.0/(1.0+x*x);
    }
  }
  pi = 0;
  for(i=0; i<num_t; i++) 
    pi += sum[i] * step;
  printf("PI approximate = %lf\n", pi);
}
