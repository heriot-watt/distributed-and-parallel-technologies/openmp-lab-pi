#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define NB_THREADS 10
#define NB_STEPS 10000

int main(int argc, char **argv) {
  double start, end, x, pi = 0, sum = 0;
  int num_t;

  num_t = argc == 2 ? strtol(argv[1], NULL, 10) : NB_THREADS;
  start = omp_get_wtime();

  #pragma omp parallel num_threads(num_t) \
    default(none) private (x) reduction(+: sum)
  {
    #pragma omp for schedule(static, 2)
    for (int i = 0; i < NB_STEPS; i++) {
      x = (i+0.5) / NB_STEPS;
      x = 4.0 / (1.0 + x*x);
      sum += x;
    }
  }

  pi = sum / NB_STEPS;
  end = omp_get_wtime();

  printf("Execution time: %.6f sec\n", end-start);
  printf("PI ~= %lf\n", pi);
  return 0;
}
