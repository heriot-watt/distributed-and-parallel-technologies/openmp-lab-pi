#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

#define NB_THREADS 10
#define NB_STEPS 10000

int main(int argc, char **argv) {
  double start, end, x, pi = 0, sum = 0;
  int num_t, num_threads, id;

  num_t = argc == 2 ? strtol(argv[1], NULL, 10) : NB_THREADS;

  omp_set_num_threads(num_t);

  start = omp_get_wtime();

  #pragma omp parallel \
    private (num_threads, id, x) \
    shared (sum)
  {
    num_threads = omp_get_num_threads();
    id = omp_get_thread_num();

    for (int i = id; i < NB_STEPS; i+=num_threads) {
      x = (i+0.5) / NB_STEPS;
      x = 4.0 / (1.0 + x*x);

      #pragma omp atomic
        sum += x;
    }
  }

  pi = sum / NB_STEPS;
  end = omp_get_wtime();

  printf("Execution time: %.6f sec\n", end-start);
  printf("PI ~= %lf\n", pi);
  return 0;
}
